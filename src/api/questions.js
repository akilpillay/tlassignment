var quizQuestions = [
  {
    question: "What color do you like your beer to be?",    
    answers: [
      {
        type: "Color1",
        content: "Pale Straw",
        answer : false
      },
      {
        type: "Color2",
        content: "Straw",
        answer : false
      },
      {
        type: "Color3",
        content: "Pale Gold",
        answer : false
      },
      {
        type: "Color4",
        content: "Deep Gold",
        answer : false
      },
      {
        type: "Color5",
        content: "Pale Amber",
        answer : false
      },
      {
        type: "Color6",
        content: "Medium Amber",
        answer : false
      },
      {
        type: "Color7",
        content: "Deep Amber",
        answer : false
      },
      {
        type: "Color8",
        content: "Amber Brown",
        answer : false
      },
      {
        type: "Color9",
        content: "Brown",
        answer : false
      },
      {
        type: "Color10",
        content: "Ruby Brown",
        answer : false
      },
      {
        type: "Color11",
        content: "Deep Brown",
        answer : false
      },
      {
        type: "Color12",
        content: "Black",
        answer : false
      }
    ]
  },
  {
    question: "What is the bitterness level you enjoy?",    
    answers: [
      {
        type: "Level1",
        content: "Low",
        answer : false
      },
      {
        type: "Level2",
        content: "Extreme",
        answer : false
      }      
    ]
  },
  {
    question: "What are the beer flavors/aroma you like?",    
    answers: [
      {
        type: "Flavor1",
        content: "Sugary",
        answer : false
      },
      {
        type: "Flavor2",
        content: "Malty & Maillard",
        answer : false
      },
      {
        type: "Flavor3",
        content: "Herbal",
        answer : false
      },
      {
        type: "Flavor4",
        content: "Aged",
        answer : false
      },
      {
        type: "Flavor5",
        content: "Floral",
        answer : false
      },
      {
        type: "Flavor6",
        content: "Fruity",
        answer : false
      },
      {
        type: "Flavor7",
        content: "Spicy",
        answer : false
      }      
    ]
  },
  {
    question: "How many countries have you visited?",    
    answers: [
      {
        type: "Count1",
        content: "-",
        answer : false
      },
      {
        type: "Count2",
        content: "+",
        answer : false
      }
    ]
  },
  {
    question: "How well does the below statement represent you? “I frequently seek out opportunities to challenge myself and grow as a person.”",
    answers: [
      {
        type: "Statement1",
        content: "Strongly disagree",
        answer : false
      },
      {
        type: "Statement2",
        content: "Disagree",
        answer : false
      },
      {
        type: "Statement3",
        content: "Neither agree nor disagree",
        answer : false
      },
      {
        type: "Statement4",
        content: "Agree",
        answer : false
      },
      {
        type: "Statement5",
        content: "Strongly Agree",
        answer : false
      }
    ]
  }
];

export default quizQuestions;